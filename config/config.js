// ref: https://umijs.org/config/
import { primaryColor } from '../src/defaultSettings';
export default {
  history: 'hash', //采用hash路由：#/xxx的形式
  base:'./',
  publicPath:'./',
  plugins: [
    [
      'umi-plugin-react',
      {
        antd: true,
        dva: { hmr: true },
        targets: { ie: 11 },
        locale: {
          enable: true,
          // default false
          default: 'zh-CN',
          // default zh-CN
          baseNavigator: true,
        },
        // default true, when it is true, will use `navigator.language` overwrite default
        dynamicImport: { loadingComponent: './components/PageLoading/index' },
      },
    ],
    [
      'umi-plugin-pro-block',
      {
        moveMock: false,
        moveService: false,
        modifyRequest: true,
        autoAddMenu: true,
      },
    ],
  ],
  targets: { ie: 11 },
  /**
   * 路由相关配置
   */
  routes: [
    {
      path: '/user',
      component: '../layouts/UserLayout',
      routes: [
        {
          path: '/user',
          component: './Welcome',
        },
      ],
    },
    {
      path: '/',
      component: '../layouts/BasicLayout',
      routes: [
        {
          path: '/',
          redirect: '/welcome',
        },
        // dashboard
        {
          path: '/welcome',
          name: 'welcome',
          icon: 'smile',
          component: './Welcome',
        },
        {
          path: 'https://github.com/umijs/umi-blocks/tree/master/ant-design-pro',
          name: 'more-blocks',
          icon: 'block',
        },
        {
          name: 'blank',
          icon: 'smile',
          path: '/blank',
          component: './blank',
        },
        {
          name: 'basic-list',
          icon: 'smile',
          path: '/basic-list',
          component: './basic-list',
        },
        {
          name: 'demo',
          icon: 'smile',
          path: '/demo',
          component: './demo',
        },
        {
          name: 'workplace',
          icon: 'smile',
          path: '/workplace',
          component: './workplace',
        },
        {
          name: 'workplace2',
          icon: 'smile',
          path: '/workplace2',
          component: './workplace',
        },
        {
          name: 'table-list',
          icon: 'smile',
          path: '/table-list',
          component: './table-list',
        },
        {
          name: 'hello-block',
          icon: 'smile',
          path: '/test_hello/hello-block',
          component: './Test_Hello/hello-Block',
        },
        {
          name: 'tj_table',
          icon: 'smile',
          path: '/tj_table',
          component: './tj_table',
        },
        {
          name: 'aticle',
          icon: 'smile',
          path: '/aticle',
          component: './aticle',
        },
      ],
    },
  ],
  disableRedirectHoist: true,
  /**
   * webpack 相关配置
   */
  define: { APP_TYPE: process.env.APP_TYPE || '' },
  // Theme for antd
  // https://ant.design/docs/react/customize-theme-cn
  theme: { 'primary-color': primaryColor },
  externals: { '@antv/data-set': 'DataSet' },
  ignoreMomentLocale: true,
  lessLoaderOptions: { javascriptEnabled: true },
};
